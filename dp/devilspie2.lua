#devilspie2 script


name = get_window_name()
app_name= get_application_name()
type=get_window_type()
class_name=get_class_instance_name()
class=get_window_class()
xid=get_window_xid()

function notify (xid, name)
   msg = string.format("{\"command\": { \"xid\": %s, \"name\": \"%s\" }}", xid, name ) 
   print(msg )
end

function debug( name, value )
   msg=string.format("{ \"%s\": \"%s\" }", name, value)
   debug_print( msg )
end


debug("name",name )
debug("app_name", app_name)
debug("type", type)
debug("class name", class_name)
debug("class", class)
debug("xid", xid)

if class == "Google-chrome" then
  if class_name == "google-chrome (~/.config/chromium-drm)" then
    notify(xid, "ent-chrome" )
  end
  if class_name == "google-chrome (~/.config/chromium-work)" then
    notify(xid, "work-chrome" )
  end
  if class_name == "google-chrome (~/.config/chromium-gmail)" then
    notify(xid, "gmail-chrome" )
  end
end

io.flush ()
